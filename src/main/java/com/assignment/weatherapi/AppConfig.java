package com.assignment.weatherapi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Application bean configuration class.
 *
 * @author Shivaji Pote
 */
@Configuration
public class AppConfig {

  /**
   * {@link RestTemplate} bean.
   *
   * @return {@code RestTemplate} instance
   */
  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }
}
