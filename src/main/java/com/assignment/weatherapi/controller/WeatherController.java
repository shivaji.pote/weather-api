package com.assignment.weatherapi.controller;

import com.assignment.weatherapi.client.OpenWeatherClient;
import com.assignment.weatherapi.model.Weather;
import com.assignment.weatherapi.model.WeatherResponse;
import com.assignment.weatherapi.service.WeatherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

/**
 * Controller class containing weather API endpoints.
 *
 * @author Shivaji Pote
 */
@RestController
@RequiredArgsConstructor
@Validated
public class WeatherController {

  private final WeatherService weatherService;

  private final OpenWeatherClient openWeatherClient;

  /**
   * This endpoint retrieves weather data from <em>Open Weather API</em> for provided city and inserts it into
   * database.
   *
   * @param city name of the city for which data needs to be fetched
   * @return {@link Weather} wrapped in {@link ResponseEntity}
   */
  @GetMapping(value = "/weather", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Weather> weather(@RequestParam @NotBlank String city) {
    WeatherResponse weatherResponse = openWeatherClient.getWeather(city);
    return ResponseEntity.ok(weatherService.saveWeather(weatherResponse));
  }

}
