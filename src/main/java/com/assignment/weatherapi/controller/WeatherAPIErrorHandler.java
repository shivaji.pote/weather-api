package com.assignment.weatherapi.controller;

import com.assignment.weatherapi.model.WeatherAPIError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * Exception handler class for Weather API app.
 *
 * @author Shivaji Pote
 **/
@ControllerAdvice
public class WeatherAPIErrorHandler {

  /**
   * This method handles {@link HttpClientErrorException} which is thrown by {@link
   * com.assignment.weatherapi.client.OpenWeatherClient#getWeather(String)} when city is not found in open weather
   * database.
   *
   * @param exception {@code HttpClientErrorException} instance thrown by {@code OpenWeatherClient}
   * @return {@link WeatherAPIError} wrapped in {@link ResponseEntity}
   */
  @ExceptionHandler({HttpClientErrorException.class})
  public ResponseEntity<WeatherAPIError> handleValidationError(HttpClientErrorException exception) {
    WeatherAPIError error = new WeatherAPIError();
    error.setStatus(HttpStatus.NOT_FOUND.value());
    error.setError(exception.getStatusText());
    error.setMessage("Weather report not found for provided city");
    return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
  }

  /**
   * This method handles {@link ConstraintViolationException} which will be throws when API input is invalid.
   *
   * @param e {@link ConstraintViolationException} instance
   * @return {@link WeatherAPIError} wrapped in {@link ResponseEntity}
   */
  @ExceptionHandler({ConstraintViolationException.class})
  public ResponseEntity<WeatherAPIError> handleConstraintException(ConstraintViolationException e) {
    ConstraintViolation violation = e.getConstraintViolations().iterator().next();
    WeatherAPIError error = new WeatherAPIError();
    error.setError(HttpStatus.BAD_REQUEST.name());
    error.setStatus(HttpStatus.BAD_REQUEST.value());
    String message = violation.getPropertyPath().toString() + " cannot be empty";
    error.setMessage(message);
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  /**
   * This method handles {@link MissingServletRequestParameterException} which is thrown when request parameter is
   * missing from request URI.
   *
   * @param e {@link MissingServletRequestParameterException} instance
   * @return {@link WeatherAPIError} wrapped in {@link ResponseEntity}
   */
  @ExceptionHandler({MissingServletRequestParameterException.class})
  public ResponseEntity<WeatherAPIError> handleRequiredParameterException(MissingServletRequestParameterException e) {
    WeatherAPIError error = new WeatherAPIError();
    error.setError(HttpStatus.BAD_REQUEST.name());
    error.setStatus(HttpStatus.BAD_REQUEST.value());
    error.setMessage(e.getMessage());
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

}
