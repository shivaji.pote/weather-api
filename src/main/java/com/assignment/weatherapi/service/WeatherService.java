package com.assignment.weatherapi.service;

import com.assignment.weatherapi.model.Weather;
import com.assignment.weatherapi.model.WeatherResponse;

/**
 * Weather service interface containing methods for manipulating weather data.
 *
 * @author Shivaji Pote
 **/
public interface WeatherService {

  /**
   * This method saves weather data in database.
   *
   * @param weatherResponse {@link WeatherResponse} containing weather data to be saved
   * @return {@link Weather} instance containing saved weather data
   */
  Weather saveWeather(WeatherResponse weatherResponse);
}
