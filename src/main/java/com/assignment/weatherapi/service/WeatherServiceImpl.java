package com.assignment.weatherapi.service;

import com.assignment.weatherapi.entity.WeatherEntity;
import com.assignment.weatherapi.model.Weather;
import com.assignment.weatherapi.model.WeatherResponse;
import com.assignment.weatherapi.repository.WeatherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

/**
 * Implementation of {@link WeatherService}.
 *
 * @author Shivaji Pote
 **/
@Service
@RequiredArgsConstructor
@Log4j2
public class WeatherServiceImpl implements WeatherService {

  private final WeatherRepository weatherRepository;

  /**
   * {@inheritDoc}
   *
   * @param weatherResponse {@link WeatherResponse} containing weather data to be saved
   * @return {@link Weather} instance containing saved weather data
   */
  @Override
  public Weather saveWeather(final @NotNull WeatherResponse weatherResponse) {
    WeatherEntity entity = new WeatherEntity();
    entity.setCity(weatherResponse.getName());
    entity.setCountry(weatherResponse.getSys().getCountry());
    entity.setTemperature(weatherResponse.getMain().getTemp());
    return mapWeather(weatherRepository.save(entity));
  }

  /**
   * This method maps weather entity data to {@link Weather}.
   *
   * @param weatherEntity weather entity
   * @return {@code Weather} instance
   */
  private Weather mapWeather(WeatherEntity weatherEntity) {
    Weather weather = new Weather();
    weather.setCity(weatherEntity.getCity());
    weather.setCountry(weatherEntity.getCountry());
    weather.setTemperature(weatherEntity.getTemperature());
    return weather;
  }
}
