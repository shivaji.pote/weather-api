
package com.assignment.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Weather response model class holding weather data returned by <em>Open Weather API</em>.
 *
 * @author Shivaji Pote
 */
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {

  @Valid
  @NotNull
  private Sys sys;

  @NotBlank
  private String name;

  @Valid
  @NotNull
  private Main main;

}
