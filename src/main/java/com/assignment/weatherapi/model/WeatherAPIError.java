package com.assignment.weatherapi.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Weather API error model class.
 *
 * @author Shivaji Pote
 **/
@Getter
@Setter
public class WeatherAPIError {

  private int status;

  private String error;

  private String message;
}
