
package com.assignment.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * Model class which holds weather data.
 *
 * @author Shivaji Pote
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Weather {

  private String city;

  private String country;

  private Double temperature;

}
