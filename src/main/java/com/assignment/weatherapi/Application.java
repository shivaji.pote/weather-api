package com.assignment.weatherapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main application class which launches weather api app.
 *
 * @author Shivaji Pote
 */
@SpringBootApplication
public class Application {

  /**
   * Main method which starts application.
   *
   * @param args application input parameters
   */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
