package com.assignment.weatherapi;

/**
 * Constants interface containing application constants.
 *
 * @author Shivaji Pote
 */
public interface Constants {

  String WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={appid}";

}
