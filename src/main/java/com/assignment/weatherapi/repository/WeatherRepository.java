package com.assignment.weatherapi.repository;

import com.assignment.weatherapi.entity.WeatherEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Weather repository interface extending JPA's {@link CrudRepository} containing basic <em>CRUD</em> operation methods.
 * Custom methods with custom queries can be put here.
 *
 * @author Shivaji Pote
 */
public interface WeatherRepository extends CrudRepository<WeatherEntity, Integer> {
}
