package com.assignment.weatherapi.client;

import com.assignment.weatherapi.model.WeatherResponse;

/**
 * Client interface for fetching weather data from Open Weather APIs.
 *
 * @author Shivaji Pote
 **/
public interface OpenWeatherClient {

  /**
   * This method retrieves weather information for specified city.
   *
   * @param city city for which weather information needs to be fetched
   * @return {@link WeatherResponse} instance containing weather details of provided city
   */
  WeatherResponse getWeather(String city);

}
