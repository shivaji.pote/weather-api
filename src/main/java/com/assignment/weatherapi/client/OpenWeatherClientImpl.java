package com.assignment.weatherapi.client;

import com.assignment.weatherapi.Constants;
import com.assignment.weatherapi.model.WeatherResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Implementation of {@link OpenWeatherClient} interface.
 *
 * @author Shivaji Pote
 **/
@RequiredArgsConstructor
@Service
@Log4j2
public class OpenWeatherClientImpl implements OpenWeatherClient {

  @Value("${openweather.api-key}")
  private String appKey;

  private final RestTemplate restTemplate;

  /**
   * {@inheritDoc}
   *
   * @param city city for which weather information needs to be fetched
   * @return {@link WeatherResponse} instance containing weather details of provided city
   */
  @Override
  public WeatherResponse getWeather(final String city) {
    log.debug("Getting weather details for city {}", city);
    String url = Constants.WEATHER_API_URL.replace("{city}", city).replace("{appid}", appKey);
    ResponseEntity<WeatherResponse> response = restTemplate.getForEntity(url, WeatherResponse.class);
    return response.getBody();
  }
}
