package com.assignment.weatherapi.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Entity class holding <em>weather</em> entity data.
 *
 * @author Shivaji Pote
 */
@Getter
@Setter
@Entity
@Table(name = "weather")
public class WeatherEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "temperature", nullable = false)
    private Double temperature;

}
