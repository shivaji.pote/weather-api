package com.assignment.weatherapi.service;

import com.assignment.weatherapi.entity.WeatherEntity;
import com.assignment.weatherapi.model.Weather;
import com.assignment.weatherapi.repository.WeatherRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import util.TestUtils;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static util.TestUtils.mockedWeatherEntity;
import static util.TestUtils.mockedWeatherResponse;


/**
 * @author Shivaji Pote
 **/
@ExtendWith(MockitoExtension.class)
class WeatherServiceImplTest {

  @InjectMocks
  private WeatherServiceImpl weatherService;

  @Mock
  private WeatherRepository weatherRepository;

  @Test
  void saveWeather_CallsSaveRepositoryMethodsToSaveWeatherData() throws IOException {
    when(weatherRepository.save(any(WeatherEntity.class))).thenReturn(mockedWeatherEntity());
    Weather savedData = weatherService.saveWeather(mockedWeatherResponse());
    assertNotNull(savedData);
    assertEquals("Amsterdam",savedData.getCity());
    assertEquals("NL",savedData.getCountry());
    assertEquals(123.45,savedData.getTemperature());

  }
}
