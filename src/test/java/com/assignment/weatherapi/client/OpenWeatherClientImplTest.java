package com.assignment.weatherapi.client;

import com.assignment.weatherapi.model.WeatherResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import util.TestUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

/**
 * @author Shivaji Pote
 **/
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class OpenWeatherClientImplTest {

  @Autowired
  private OpenWeatherClient openWeatherClient;

  @Autowired
  private RestTemplate restTemplate;

  private MockRestServiceServer mockServer;

  private ObjectMapper mapper = new ObjectMapper();

  @BeforeEach
  public void setup() {
    mockServer = MockRestServiceServer.createServer(restTemplate);
  }


  @Test
  void getWeather_RetrievesWeatherDataFromOpenWeatherAPIsWhenValidCityIsPassed() throws IOException, URISyntaxException {
    mockServer.expect(ExpectedCount.once(),
      requestTo(new URI("http://api.openweathermap.org/data/2.5/weather?q=amsterdam&APPID=f3ae8a946be7c828abf9a5580c32aa92")))
      .andExpect(method(HttpMethod.GET))
      .andRespond(withStatus(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(mapper.writeValueAsString(TestUtils.mockedWeatherResponse()))
      );
    WeatherResponse response = openWeatherClient.getWeather("amsterdam");
    assertNotNull(response);
    assertEquals("Amsterdam", response.getName());
    assertEquals("NL", response.getSys().getCountry());
    assertEquals(269.12, response.getMain().getTemp());
  }

  @Test
  void getWeather_ThrowsClientExceptionWhenProvidedCityIsNotFoundInOpenWeatherAPI() throws IOException, URISyntaxException {
    mockServer.expect(ExpectedCount.once(),
      requestTo(new URI("http://api.openweathermap.org/data/2.5/weather?q=aa&APPID=f3ae8a946be7c828abf9a5580c32aa92")))
      .andExpect(method(HttpMethod.GET))
      .andRespond(withStatus(HttpStatus.NOT_FOUND)
        .contentType(MediaType.APPLICATION_JSON));
    assertThrows(HttpClientErrorException.class, () -> openWeatherClient.getWeather("aa"));
  }
}
