package com.assignment.weatherapi.controller;

import com.assignment.weatherapi.client.OpenWeatherClient;
import com.assignment.weatherapi.model.Weather;
import com.assignment.weatherapi.model.WeatherResponse;
import com.assignment.weatherapi.service.WeatherService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.HttpClientErrorException;
import util.TestUtils;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static util.TestUtils.mockedWeather;
import static util.TestUtils.mockedWeatherResponse;

/**
 * @author Shivaji Pote
 **/
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class WeatherControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private OpenWeatherClient openWeatherClient;

  @MockBean
  private WeatherService weatherService;

  @Test
  void weather_RetrievesWeatherFromOpenWeatherAndSavesInDatabase() throws Exception {
    when(openWeatherClient.getWeather(anyString())).thenReturn(mockedWeatherResponse());
    final Weather response = mockedWeather();
    when(weatherService.saveWeather(Mockito.any(WeatherResponse.class))).thenReturn(response);
    mockMvc.perform(MockMvcRequestBuilders.get("/weather?city=amsterdam").contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andExpect(MockMvcResultMatchers.content().string(TestUtils.asJsonString(response)));
  }

  @Test
  void weather_Throws404WhenOpenWeatherThrowsHttpClientErrorException() throws Exception {
    when(openWeatherClient.getWeather(anyString())).thenThrow(HttpClientErrorException.class);
    mockMvc.perform(MockMvcRequestBuilders.get("/weather?city=aa").contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  void weather_Throws400WhenInputCityIsEmpty() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/weather?city=").contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  void weather_Throws400WhenCityParameterIsNotPassed() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/weather?").contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }
}
