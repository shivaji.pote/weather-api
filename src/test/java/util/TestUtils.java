package util;

import com.assignment.weatherapi.entity.WeatherEntity;
import com.assignment.weatherapi.model.Weather;
import com.assignment.weatherapi.model.WeatherResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Shivaji Pote
 **/
public final class TestUtils {

  private static ObjectMapper objectMapper = new ObjectMapper();

  public static WeatherResponse mockedWeatherResponse() throws IOException {
    return objectMapper.readValue(readFile("open-weather-response.json"), WeatherResponse.class);
  }

  private static InputStream readFile(final String fileName) {
    return Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
  }

  private TestUtils() throws InstantiationException {
    throw new InstantiationException("Cannot instantiate util class");
  }

  public static WeatherEntity mockedWeatherEntity() {
    WeatherEntity weatherEntity = new WeatherEntity();
    weatherEntity.setCity("Amsterdam");
    weatherEntity.setCountry("NL");
    weatherEntity.setTemperature(123.45);
    return weatherEntity;
  }

  public static Weather mockedWeather() {
    Weather weather = new Weather();
    weather.setCity("Amsterdam");
    weather.setCountry("NL");
    weather.setTemperature(123.45);
    return weather;
  }

  /*
   * converts a Java object into JSON representation
   */
  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (final Exception e) {
      throw new RuntimeException(e);
    }
  }
}
