Spring Boot Coding Dojo
---

Welcome to the Spring Boot Coding Dojo!

### Introduction

This is a simple application that requests its data from [OpenWeather](https://openweathermap.org/) and stores the result in a database.
This is refactored production ready implementation.


### Functionality
This application exposes below REST endpoint/s.

_**GET :** /weather?city={cityName}_

    This endpoint retrieves weather data from Free Open Weather APIs and stores in database.
    
    Sample Response :
    {
      "city": "Amsterdam",
      "country": "NL",
      "temperature": 276.27
    }


### How to Run Application
  1. Clone repository by executing below command 
     
    git clone https://gitlab.com/shivaji.pote/weather-api.git
  
  2. Go to directory where you have checked out code and run below commands

    mvn clean install

    mvn spring-boot:run
  Second command will launch application. Once application is started, you can access it using below URL:

    http://localhost:8080/weather?city=amsterdam

**_Note_**: If port 8080 is already being used on your machine, please add/change value of server.port in _src/main/resources/application.properties_ to something else and try again.


### Footnote
This application uses Open Weather API key of account _shivaji.pote@gmail.com_ 
